import React from 'react';
import { View, StyleSheet } from 'react-native';

const Card = ({children}) => {
  return (
    <View style={styles.containerStyle}>
      {children}
    </View>
  )
};
const styles = StyleSheet.create({
  containerStyle: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    margin: 5,
  }
});

export default Card;
