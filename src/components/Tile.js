import React from 'react';
import { Tile } from 'react-native-elements';

const MyTile = () => {
  return (
    <Tile
      imageSrc={require('../img/tile.jpg')}
      title="Когда больше нечего ждать и не на что надеяться"
      featured
      caption="Найдем еще причины для паники"
    />
  )
};

export default MyTile;
