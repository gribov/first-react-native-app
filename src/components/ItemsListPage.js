import React, { Component } from 'react';
import { ScrollView } from 'react-native';

import Api from '../core/api/fearsApi';
import ItemDetail from './ItemDetail';
import Tile from './Tile';

class ItemsListPage extends Component {
  state = { items: [] };

  componentDidMount() {
    Api.getAll().then(items => {
      this.setState({ items });
    });
  }

  render() {
    if (!this.state.items.length) {
      return null;
    }
    return (
      <ScrollView>
        {this.state.items.map(item => <ItemDetail key={item.id} item={item}/>)}
        <Tile />
      </ScrollView>
    );
  }
}

export default ItemsListPage;
