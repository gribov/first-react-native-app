import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

import stylesConfig from '../stylesConfig';

type HeaderProps = {
  headerText: string,
}

class Header extends Component<HeaderProps> {
  render() {
    return (
      <View style={styles.wrapperStyles}>
        <Text style={styles.textStyles}>{this.props.headerText}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  textStyles: {
    color: '#ffffff',
    fontSize: 25
  },
  wrapperStyles: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: stylesConfig.mainColor,
    height: 120,
  }
});

export default Header;
