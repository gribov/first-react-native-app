import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import FearListItem from './FearListItem';
import { loadFears } from '../state/actions';

class FearsList extends Component {

  componentDidMount() {
    this.props.loadFears()
  }

  render() {
    if (!this.props.fears.length) {
      return null;
    }
    return (
      <View style={{ marginBottom: 20 }}>
        {this.props.fears.map(fear => <FearListItem key={fear.id} item={fear}/>)}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    fears: state.fearsList
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ loadFears }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FearsList);
