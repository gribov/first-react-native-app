import React from 'react';
import { Text, View } from 'react-native';
import { Badge } from 'react-native-elements';

const StatusBar = () => {
  return (
    <View>
      <Badge status="error" value={'+8 новых опасений'}/>
    </View>
  )
};

export default StatusBar;
