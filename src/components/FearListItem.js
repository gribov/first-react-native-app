import React from 'react';
// import { View, Image, Text } from 'react-native';
import { ListItem } from 'react-native-elements';

const FearListItem = ({item}) => {
  return (
    <ListItem
      roundAvatar
      leftAvatar={{ source: { uri: item.image.src } }}
      key={item.id}
      title={item.name}
      subtitle={item.description}
    />
  )
};

export default FearListItem;
