import React from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise';

import reducers from '../state/reducers'

import Header from './Header';
import ItemsListPage from './ItemsListPage';

import StatusBar from './StatusBar';
import FearsList from './FearsList';

const store = createStore(reducers, applyMiddleware(promise));

const App = () => (
  <Provider store={store}>
    <View style={{ flex: 1 }}>
      <Header headerText="Мои страхи"/>
      <StatusBar />
      <ItemsListPage />
    </View>
  </Provider>
);

export default App;
