import React from "react";
import { PricingCard } from "react-native-elements";

export default () => (
  <PricingCard
    color="#f28ab7"
    title="Новые загоны"
    price="$9.99"
    containerStyle={{marginBottom: 90}}
    info={['5 Новых идей для Тараканов в Вашей голове']}
    button={{ title: 'Купить' }}
  />
)
