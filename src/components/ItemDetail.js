import React from 'react';
import { View, Text, StyleSheet, Image, Alert } from 'react-native';

import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

const ItemDetail = ({ item }) => {
  const { name, description, image: { src: uri } } = item;
  const {
    thumbnailStyle,
    headerContentStyle,
    headerTextStyle,
    thumbnailContainerStyle,
    imageStyle,
  } = styles;

  const onPress = () => {
    Alert.alert(
      'Подтвердите',
      `Вас все еще беспокоит ${item.name} ?`,
      [
        { text: 'Да', onPress: () => console.log('OK Pressed') },
        { text: 'Нет', onPress: () => console.log('Ask me later pressed') },
      ],
      { cancelable: false },
    )
  };

  return (
    <Card>
      <CardSection>
        <View style={thumbnailContainerStyle}>
          <Image
            source={{ uri }}
            style={thumbnailStyle}
          />
        </View>
        <View styles={headerContentStyle}>
          <Text style={headerTextStyle}>{name}</Text>
          <Text>{description}</Text>
        </View>
      </CardSection>
      <CardSection>
        <Image style={imageStyle} source={{ uri }}/>
      </CardSection>
      <CardSection>
        <Button text="Проверить" onPress={onPress}/>
      </CardSection>
    </Card>
  )
};

const styles = StyleSheet.create({
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  headerTextStyle: {
    fontSize: 16,
  },
  thumbnailStyle: {
    width: 50,
    height: 50,
    borderRadius: 5,
  },
  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 2,
    marginRight: 10,
  },
  imageStyle: {
    flex: 1,
    height: 300,
    width: null,
  }
});

export default ItemDetail;
