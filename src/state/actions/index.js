import fearsApi from '../../core/api/fearsApi';

export const LOAD_FEARS_LIST = 'LOAD_FEARS_LIST';

export function loadFears() {
  return {
    type: LOAD_FEARS_LIST,
    payload: fearsApi.getAll()
  }
}
