import { combineReducers } from 'redux'

import fearsListReducer from './fearsListReducer'

export default combineReducers({
  fearsList: fearsListReducer
})
