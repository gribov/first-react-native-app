import { LOAD_FEARS_LIST } from '../actions'

export default function (state = [], action) {
  console.log('REDUCER::', action);
  switch (action.type) {
    case LOAD_FEARS_LIST:
      return action.payload;
    default:
      return state;
  }
}
