const API_HOST = 'http://localhost:9002'; // http://10.0.2.2:9002 for android simulator

import fearsData from './fearsStub.json';

export default class Api {
  constructor(endpoint) {
    this.endpoint = endpoint;
  }

  getAll() {
    if (this.endpoint === 'fears') {
      return Promise.resolve(fearsData);
    }
    return Promise.resolve();
    //return fetch(`${API_HOST}/api/${this.endpoint}/`)
      //.then(res => res.json());
  }
}

