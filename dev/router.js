const fs = require('fs');
const path = require('path');

module.exports = function (app) {
    app.get('/api/fears/', function (req, res, next) {
        res.statusCode = 200;
        return fs.readFile(path.resolve(__dirname, 'data', 'fears.json'), function (err, data) {
            return res.json(JSON.parse(data));
        });
    });
};
