const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const router = require('./router');
const cors = require('cors');

//init App
const app = express();
//App setup
app.use(cors({origin: '*'}));
app.use(bodyParser.json({type: '*/*'}));

router(app);

//Server setup
const port = process.env.PORT || 9002
const server = http.createServer(app);
server.listen(port);
console.log('SERVER LISTENING ON PORT ', port);
