Инструкция по установке основных зависимостей (https://facebook.github.io/react-native/docs/getting-started.html)
Выбрать `React Native CLI Quickstart`
```
brew install node
brew install watchman
brew tap AdoptOpenJDK/openjdk
brew cask install adoptopenjdk8
```
```
npm install -g react-native-cli
npm install
react-native run-ios
```

ReactNative Redux TypeScript

![alt text](/img.jpg)